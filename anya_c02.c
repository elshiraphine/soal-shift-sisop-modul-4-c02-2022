#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>

char workdir[] = "/home/elshervnn/workdir";

// int check2ndcommand( char *fpath)
// {
//     FILE* f = fopen("/home/elshervnn/hayolongapain_c02.log", "r");
//     char line[1000];
//     char *token, *type;
//     while(fgets(line, sizeof(line), f))
//     {
//         token = strtok(line, " ");
//         type = token;

//         token = strtok(NULL, " ");
//         token = strtok(NULL, " ");
//         token = strtok(NULL, " ");
//         token[strlen(token) -1] = 0;

//         if (strncmp(token, fpath, strlen(token)))
//         {
//             if (!strncmp("RENAME", type, 6))
//             {
//                 fclose(f);
//                 return 3;
//             }

//             if (!strncmp("MKDIR", type, 5))
//             {
//                 fclose(f);
//                 return 4;
//             }
//         }
//     }

//     fclose(f);
//     return -1;
// }

int checkencrypt(char *fpath)
{
    char temp[10000];
    strcpy(temp, fpath);

    char *token = strtok(temp, "/");
    while (token != NULL)
    {
        if (!strncmp("Animeku_", token, 8) == 0)
            return 1;

        if (!strncmp("IAN_", token, 4))
            return 2;

        if (!strncmp("nam_do-saq_", token, 11))
            return 3;

        token = strtok(NULL, "/");
    }

    return 0;
}

int type(const char *path)
{
    struct stat statbuf;
    if (stat(path, &statbuf) == -1)
        return -1;

    return S_ISDIR(statbuf.st_mode);
}

char *cipher(char *filename)
{
    int extlen = 0;
    char *ext = strrchr(filename, '.');
    if (ext)
    {
        extlen = strlen(ext);
    }

    for (int i = 0; i < strlen(filename) - extlen; i++)
    {
        if (filename[i] >= 'a' && filename[i] <= 'z')
        {
            // rot 13
            filename[i] = (filename[i] + 13 - 'a') % 26 + 'a';
        }
        if (filename[i] >= 'A' && filename[i] <= 'Z')
        {   
            // atbash
            filename[i] = 'Z' + 'A' - filename[i];
        }
    }

    return filename;
}

char *decvigenere(char *encrypted)
{
    char key[] = "INNUGANTENG";
    int msgLen = strlen(encrypted), keyLen = strlen(key), i, j, extlen = 0;
    char newKey[msgLen], decrypted[msgLen];

    char *ext = strrchr(encrypted, '.');
    if (ext)
    {
        extlen = strlen(ext);
    }

    for (i = 0, j = 0; i < msgLen - extlen; ++i, ++j)
    {
        if (j == keyLen)
            j = 0;

        newKey[i] = key[j];
    }

    // decryption
    for (i = 0; i < msgLen - extlen; ++i)
    {
        if (encrypted[i] >= 'a' && encrypted[i] <= 'z')
            decrypted[i] = (((encrypted[i] - newKey[i]) + 26 - ('a' - 'A')) % 26) + 'a';
        else if (encrypted[i] >= 'A' && encrypted[i] <= 'Z')
            decrypted[i] = (((encrypted[i] - newKey[i]) + 26) % 26) + 'A';
        else
            decrypted[i] = encrypted[i];
        // decrypted[i] = (((encrypted[i] - newKey[i]) + 26) % 26) + 'A';
    }

    for (; i < msgLen; ++i)
    {
        decrypted[i] = encrypted[i];
    }

    decrypted[i] = '\0';

    // strcpy(decrypted, enkripsi_atbash(decrypted));

    strcpy(encrypted, decrypted);
    return encrypted;
}

char *vigenere(char *msg)
{
    char key[] = "INNUGANTENG";
    int msgLen = strlen(msg), keyLen = strlen(key), i, j, extlen = 0;

    char *ext = strrchr(msg, '.');
    if (ext)
    {
        extlen = strlen(ext);
    }

    char newKey[msgLen], encrypted[msgLen];

    // generating new key
    for (i = 0, j = 0; i < msgLen; ++i, ++j)
    {
        if (j == keyLen)
            j = 0;

        newKey[i] = key[j];
    }

    newKey[i] = '\0';

    // encryption
    for (i = 0; i < msgLen - extlen; ++i)
    {
        if (msg[i] >= 'a' && msg[i] <= 'z')
            encrypted[i] = ((msg[i] + newKey[i] - ('a' - 'A')) % 26) + 'a';
        else if (msg[i] >= 'A' && msg[i] <= 'Z')
            encrypted[i] = ((msg[i] + newKey[i]) % 26) + 'A';
        else
            encrypted[i] = msg[i];
    }

    for (; i < msgLen; ++i)
    {
        encrypted[i] = msg[i];
    }

    encrypted[i] = '\0';
    strcpy(msg, encrypted);
    return msg;
}

char *enspecial(char *filename)
{
    int length_extension = 0;
    char *extension = strrchr(filename, '.');
    if (extension)
    {
        length_extension = strlen(extension);
    }

    int pengkali = 1;
    int total = 0;
    for (int i = strlen(filename) - length_extension - 1; i >= 0; i--)
    {
        if (filename[i] >= 'a' && filename[i] <= 'z')
        {
            filename[i] = ('A' + (filename[i] - 'a'));
            total += pengkali;
        }
        pengkali = pengkali << 1;
    }

    // printf("%s.%d\n", filename, total);
    return filename;
}

char *decspecial(char *filename)
{
    int declen = 0, num = 0;
    char *dec = strrchr(filename, '.');
    if (dec)
    {
        declen = strlen(dec);
        num = atoi(dec + 1);
    }

    char temp[1000] = {0};
    strncpy(temp, filename, strlen(filename) - declen);
    temp[strlen(filename) - declen] = '\0';

    int extlen = 0;
    char *ext = strrchr(temp, '.');
    if (ext)
        extlen = strlen(ext);

    int ctr = 0;
    for (int i = strlen(temp) - extlen - 1; i >= 0; i--)
    {
        if ((1 << ctr) & num)
            temp[i] = 'a' + temp[i] - 'A';

        ctr++;
    }

    strcpy(filename, temp);
    return filename;
}

void recursive(char fpath[1000], int cmd)
{
    if (!strlen(fpath))
        return;

    DIR *d;
    struct dirent *dir;

    d = opendir(fpath);
    if (!d)
        return;

    while ((dir = readdir(d)) != NULL)
    {
        if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
        {
            continue;
        }

        char newname[100], tmp[100], path[100];
        strcpy(tmp, dir->d_name);

        if (cmd == 1)
        {
            sprintf(newname, "%s/%s", fpath, cipher(tmp));
        }
        else if (cmd == 2)
        {
            sprintf(newname, "%s/%s", fpath, vigenere(tmp));
        }
        else if (cmd == 3)
        {
            sprintf(newname, "%s/%s", fpath, decvigenere(tmp));
        }

        strcpy(path, fpath);
        strcat(path, "/");
        strcpy(path, dir->d_name);

        rename(path, newname);
        if (type(newname) == 1)
        {
            recursive(newname, cmd);
            continue;
        }
    }
    closedir(d);
}

char *process(char *filename)
{
    char *prompt;
    prompt = strchr(filename, '/') + 1;

    while (prompt != NULL)
    {
        if (!strncmp(prompt, "Animeku_", 8))
        {
            if (strchr(prompt, '/'))
            {
                prompt = strchr(prompt, '/') + 1;
                int index = prompt - filename;
                cipher(prompt);
                for (int i = 0; i < strlen(prompt); i++)
                {
                    filename[index + i] = prompt[i];
                }
                filename[index + strlen(prompt)] = '\0';
                break;
            }
        }

        if (!strncmp(prompt, "IAN_", 4))
        {
            if (strchr(prompt, '/'))
            {
                prompt = strchr(prompt, '/') + 1;
                int index = prompt - filename;
                // int pos = check2ndcommand(filename);
                decvigenere(prompt);
                for (int i = 0; i < strlen(prompt); i++)
                {
                    filename[index + i] = prompt[i];
                }

                filename[index + strlen(prompt)] = '\0';
                break;
            }
        }

        if (!strncmp(prompt, "nam_do-saq_", 11))
        {
            if (strchr(prompt, '/'))
            {
                prompt = strchr(prompt, '/') + 1;
                int index = prompt - filename;

                decspecial(prompt);

                for (int i = 0; i < strlen(prompt); i++)
                {
                    filename[index + i] = prompt[i];
                }

                filename[index + strlen(prompt)] = '\0';
                break;
            }
        }

        if (strchr(prompt, '/') == NULL)
            break;

        prompt = strchr(prompt, '/') + 1;
    }

    return filename;
}

void logsatu(char *from, char *to, int type)
{
    FILE *fileout;
    fileout = fopen("/home/elshervnn/Wibu.log", "a");

    if (type == 1)
        fprintf(fileout, "RENAME terenkripsi %s -> %s\n", from, to);

    if (type == 2)
        fprintf(fileout, "RENAME terdecode %s -> %s", from, to);

    if (type == 3)
        fprintf(fileout, "MKDIR: %s -> %s", from, to);

    fclose(fileout);
}

// void logsatudecode (char *from, char *to)
// {
//     FILE *fileout;
//     fileout = fopen("/home/elshervnn/Wibu.log", "a");
//     fprintf(fileout, "RENAME terdecode %s -> %s", from, to);
//     fclose(fileout);
// }

// void logdua(char *from, char *to, int type)
// {
//     FILE *fileout;
//     fileout = fopen("/home/elshervnn/hayolongapain_c02.log", "a");

//     if (type == 1)
//         fprintf(fileout, "MKDIR: %s -> %s\n", from, to);

//     if (type == 2)
//         fprintf(fileout, "RENAME: %s -> %s\n", from, to);
// }

void appendlog(char *level, char *command, char *desc)
{
    char time_now[100] = {0};
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(time_now, 100, "%d%m%Y-%H:%M:%S", tm);

    FILE *fileout;
    fileout = fopen("/home/elshervnn/hayolongapain_c02.log", "a");
    fprintf(fileout, "%s::%s:%s::%s\n", level, time_now, command, desc);
    fclose(fileout);
}

void combinepath(const char *a, char *b, char *c)
{
    if (strcmp(a, "/") == 0)
    {
        a = b;
        sprintf(c, "%s", a);
    }
    else
    {
        sprintf(c, "%s%s", b, a);
    }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);

    res = lstat(process(fpath), stbuf);
    if (res == -1)
        return -errno;

    char desc[1000] = {0};
    strcpy(desc, fpath);
    appendlog("INFO", "GETATTR", desc);

    return 0;
}

static int xmp_access(const char *path, int mask)
{
    int res;
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);

    res = access(process(fpath), mask);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readlink(const char *path, char *buf, size_t size)
{
    int res;
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);

    res = readlink(process(fpath), buf, size - 1);
    if (res == -1)
        return -errno;

    buf[res] = '\0';

    char desc[1000] = {0};
    strcpy(desc, fpath);
    appendlog("INFO", "READLINK", desc);
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);

    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    dp = opendir(process(fpath));

    if (dp == NULL)
        return -errno;

    int flag = checkencrypt(fpath);

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char temp[10000] = {0};
        strcpy(temp, de->d_name);

        if (flag == 1)
        {
            if (de->d_type == DT_REG)
            {
                cipher(temp);
            }
            else if (de->d_type == DT_DIR && strcmp(temp, ".") != 0 && strcmp(temp, "..") != 0)
            {
                cipher(temp);
            }
        }
        if (flag == 2)
        {
            if (de->d_type == DT_REG)
            {

                vigenere(temp);
            }
            else if (de->d_type == DT_DIR && strcmp(temp, ".") != 0 && strcmp(temp, "..") != 0)
            {
                vigenere(temp);
            }
        }
        else if (flag == 3)
        {
            if (de->d_type == DT_REG)
            {
                enspecial(temp);
            }
            else if (de->d_type == DT_DIR && strcmp(temp, ".") != 0 && strcmp(temp, "..") != 0)
            {
                enspecial(temp);
            }
        }

        if (filler(buf, temp, &st, 0))
            break;
    }
    char desc[1000] = {0};
    sprintf(desc, "%s", fpath);
    appendlog("INFO", "READDIR", desc);
    closedir(dp);
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    int res;
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);
    process(fpath);

    if (S_ISREG(mode))
    {
        res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
        if (res >= 0)
            res = close(res);
    }
    else if (S_ISFIFO(mode))
        res = mkfifo(fpath, mode);
    else
        res = mknod(fpath, mode, rdev);
    if (res == -1)
        return -errno;
    char desc[1000] = {0};
    strcpy(desc, fpath);
    appendlog("INFO", "MKNOD", desc);
    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);

    res = mkdir(process(fpath), mode);
    if (res == -1)
        return -errno;

    char *tmp = strrchr(fpath, '/') + 1;

    if (!strncmp(tmp, "Animeku_", 8))
    {
        logsatu(workdir, fpath, 3);
    }

    if (!strncmp(tmp, "IAN_", 4))
    {
        logsatu(workdir, fpath, 3);
    }
    char desc[1000] = {0};
    strcpy(desc, fpath);
    appendlog("INFO", "MKDIR", desc);
    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);

    res = unlink(process(fpath));
    if (res == -1)
        return -errno;

    char desc[1000] = {0};
    strcpy(desc, fpath);
    appendlog("WARNING", "UNLINK", desc);
    return 0;
}

static int xmp_rmdir(const char *path)
{
    int res;
    char fpath[1000] = {0};
    combinepath(path, workdir, fpath);

    res = rmdir(process(fpath));
    if (res == -1)
        return -errno;

    char desc[1000] = {0};
    strcpy(desc, fpath);
    appendlog("WARNING", "RMDIR", desc);

    return 0;
}

static int xmp_symlink(const char *from, const char *to)
{
    int res;

    res = symlink(from, to);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[100] = {0}, fto[100] = {0};
    combinepath(from, workdir, ffrom);
    combinepath(to, workdir, fto);

    res = rename(ffrom, fto);
    if (res == -1)
        return -errno;

    char desc[1000] = {0};
    sprintf(desc, "%s::%s", ffrom, fto);
    appendlog("INFO", "RENAME", desc);

    if (!strncmp("/Animeku_", from, 9) && !strncmp("Animeku_", to, 9))
    {
    }

    else if (!strncmp("/Animeku_", from, 9) || !strncmp("/IAN_", from, 5))
    {
        logsatu(ffrom, fto, 1);
    }

    else if (!strncmp("Animeku_", to, 9) || !strncmp("/IAN_", to, 5))
    {
        logsatu(ffrom, fto, 2);
    }
    recursive(fto, 1);

    return 0;
}

static int xmp_link(const char *from, const char *to)
{
	int res;
 
	res = link(from, to);
	if (res == -1)
		return -errno;
 
	return 0;
}
 
static int xmp_chmod(const char *path, mode_t mode)
{
	int res;
    char fpath[1000]={0};
    combinepath(path,workdir,fpath);
 
	res = chmod(process(fpath), mode);
	if (res == -1)
		return -errno;

	char desc[1000] = {0};
	sprintf(desc,"%s",fpath);
	appendlog("INFO","CHMOD",desc);
 
	return 0;
}
 
static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
	int res;
    char fpath[1000]={0};
    combinepath(fpath,workdir,fpath);
 
	res = lchown(process(fpath), uid, gid);
	if (res == -1)
		return -errno;
	
	char desc[1000] = {0};
	sprintf(desc,"%s",fpath);
	appendlog("INFO","CHOWN",desc);
	return 0;
}
 
static int xmp_truncate(const char *path, off_t size)
{
	int res;
    char fpath[1000]={0};
    combinepath(path,workdir,fpath);
 
	res = truncate(process(fpath), size);
	if (res == -1)
		return -errno;

	char desc[1000] = {0};
	sprintf(desc,"%s",fpath);
	appendlog("INFO","TRUNCATE",desc);
	return 0;
}
 
static int xmp_utimens(const char *path, const struct timespec ts[2])
{
	int res;
    char fpath[1000]={0};
    combinepath(path,workdir,fpath);
 
	/* don't use utime/utimes since they follow symlinks */
	res = utimensat(0, process(fpath), ts, AT_SYMLINK_NOFOLLOW);
	if (res == -1)
		return -errno;
 
	return 0;
}
 
 
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
	int res;
    char fpath[1000]={0};
    combinepath(path,workdir,fpath);
 
	res = open(process(fpath), fi->flags);
	if (res == -1)
		return -errno;
 
	char desc[1000] = {0};
	strcpy(desc,fpath);
	appendlog("INFO","OPEN",desc);
	close(res);
	return 0;
}
 
static int xmp_read(const char *path, char *buf, size_t size, off_t offset,
		    struct fuse_file_info *fi)
{
	int fd;
	int res;
    char fpath[1000]={0};
    combinepath(path,workdir,fpath);
 
	(void) fi;
	fd = open(process(fpath), O_RDONLY);
	if (fd == -1)
		return -errno;
 
	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	char desc[1000] = {0};
	sprintf(desc,"%s",fpath);
	appendlog("INFO","READ",desc);
 
	close(fd);
	return res;
}
 
static int xmp_write(const char *path, const char *buf, size_t size,
		     off_t offset, struct fuse_file_info *fi)
{
	int fd;
	int res;
    char fpath[1000]={0};
 
    combinepath(path,workdir,fpath);
 
	(void) fi;
	fd = open(process(fpath), O_WRONLY);
	if (fd == -1)
		return -errno;
 

	res = pwrite(fd, buf, size, offset);
	if (res == -1)
		res = -errno;
	
	char desc[1000] = {0};
	strcpy(desc,path);
	appendlog("INFO","WRITE",desc);
	
	close(fd);
	return res;
}
 
static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
	int res;
    char fpath[1000]={0};
    combinepath(path,workdir,fpath);
 
	res = statvfs(process(fpath), stbuf);
	if (res == -1)
		return -errno;
 
	return 0;
}
 
static int xmp_create(const char* path, mode_t mode, struct fuse_file_info* fi) 
{
	char fpath[1000];
	combinepath(path,workdir,fpath);
    (void) fi;
 
    int res;
    res = creat(process(fpath), mode);
    if(res == -1) return -errno;
 
	char desc[1000] = {0};
	strcpy(desc,fpath);
	appendlog("INFO","CREATE",desc);
    close(res);
 
    return 0;
}
 
static int xmp_release(const char *path, struct fuse_file_info *fi)
{
	(void) path;
	(void) fi;
	return 0;
}
 
static int xmp_fsync(const char *path, int isdatasync,
		     struct fuse_file_info *fi)
{
	(void) path;
	(void) isdatasync;
	(void) fi;
	return 0;
}

static struct fuse_operations xmp_oper = {
	.getattr	= xmp_getattr,
	.access		= xmp_access,
	.readlink	= xmp_readlink,
	.readdir	= xmp_readdir,
	.mknod		= xmp_mknod,
	.mkdir		= xmp_mkdir,
	.symlink	= xmp_symlink,
	.unlink		= xmp_unlink,
	.rmdir		= xmp_rmdir,
	.rename		= xmp_rename,
	.link		= xmp_link,
	.chmod		= xmp_chmod,
	.chown		= xmp_chown,
	.truncate	= xmp_truncate,
	.utimens	= xmp_utimens,
	.open		= xmp_open,
	.read		= xmp_read,
	.write		= xmp_write,
	.statfs		= xmp_statfs,
    .create     = xmp_create,
	.release	= xmp_release,
	.fsync		= xmp_fsync,
};
 
int main(int argc, char *argv[])
{
	umask(0);
	return fuse_main(argc, argv, &xmp_oper, NULL);
}