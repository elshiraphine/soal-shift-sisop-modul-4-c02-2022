# soal-shift-sisop-modul-4-C02-2022

## Anggota Kelompok
1. Elshe Erviana Angely 5025201050

## Soal 1
### 1 A
Pada direktori yang berawalan `Animeku_` akan dilakukan encode dengan ketentuan: <br />
1. Huruf besar terencode atbash cipher
2. Huruf kecil terencode rot 13
```c
char *cipher(char *filename)
{
    int extlen = 0;
    char *ext = strrchr(filename, '.');
    if (ext)
    {
        extlen = strlen(ext);
    }

    for (int i = 0; i < strlen(filename) - extlen; i++)
    {
        if (filename[i] >= 'a' && filename[i] <= 'z')
        {
            // rot 13
            filename[i] = (filename[i] + 13 - 'a') % 26 + 'a';
        }
        if (filename[i] >= 'A' && filename[i] <= 'Z')
        {   
            // atbash
            filename[i] = 'Z' + 'A' - filename[i];
        }
    }

    return filename;
}
```
![testcipher](https://media.discordapp.net/attachments/964890423946543124/975406947824173076/unknown.png)
### 1 B, D, E
Semua direktori dengan awalan `Animeku_` akan terencode dengan ketentuan sama dengan **1A**. Kebalikannya, jika direkotri di-rename menjadi tidak terencode maka isi direktori akan terdecode. Berlaku secara rekursif.
```c
void recursive(char fpath[1000], int cmd)
{
    if (!strlen(fpath))
        return;

    DIR *d;
    struct dirent *dir;

    d = opendir(fpath);
    if (!d)
        return;

    while ((dir = readdir(d)) != NULL)
    {
        if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
        {
            continue;
        }

        char newname[100], tmp[100], path[100];
        strcpy(tmp, dir->d_name);

        if (cmd == 1)
        {
            sprintf(newname, "%s/%s", fpath, cipher(tmp));
        }
        else if (cmd == 2)
        {
            sprintf(newname, "%s/%s", fpath, vigenere(tmp));
        }
        else if (cmd == 3)
        {
            sprintf(newname, "%s/%s", fpath, decvigenere(tmp));
        }

        strcpy(path, fpath);
        strcat(path, "/");
        strcpy(path, dir->d_name);

        rename(path, newname);
        if (type(newname) == 1)
        {
            recursive(newname, cmd);
            continue;
        }
    }
    closedir(d);
}

```

### 1 C
Diminta untuk membuat file log yang berisi: <br />
```txt
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
```

Code <br />
```c
void logsatu(char *from, char *to, int type)
{
    FILE *fileout;
    fileout = fopen("/home/elshervnn/Wibu.log", "a");

    if (type == 1)
        fprintf(fileout, "RENAME terenkripsi %s -> %s\n", from, to);

    if (type == 2)
        fprintf(fileout, "RENAME terdecode %s -> %s", from, to);

    if (type == 3)
        fprintf(fileout, "MKDIR: %s -> %s", from, to);

    fclose(fileout);
}
```
```txt
Catatan: masih terdapat error pada filesystem sehingga tidak dapat mengenerate log. Diduga pada fungsi type tidak bisa menentukan jenis type yang dimaksud.
```
### Demo
![soal-1](https://media.discordapp.net/attachments/964890423946543124/975398821435682876/unknown.png?width=1015&height=660)

### Catatan
Untuk soal 1, direktori yang dibuat error, karena token yang dipassing dari `workdir` ke direktori `mount` tidak sama.

## Soal 2
### 2 A B C
Membuat fungsi decode dan encode untuk directori berawalan `IAN_`. <br />
```c
char *decvigenere(char *encrypted)
{
    char key[] = "INNUGANTENG";
    int msgLen = strlen(encrypted), keyLen = strlen(key), i, j, extlen = 0;
    char newKey[msgLen], decrypted[msgLen];

    char *ext = strrchr(encrypted, '.');
    if (ext)
    {
        extlen = strlen(ext);
    }

    for (i = 0, j = 0; i < msgLen - extlen; ++i, ++j)
    {
        if (j == keyLen)
            j = 0;

        newKey[i] = key[j];
    }

    // decryption
    for (i = 0; i < msgLen - extlen; ++i)
    {
        if (encrypted[i] >= 'a' && encrypted[i] <= 'z')
            decrypted[i] = (((encrypted[i] - newKey[i]) + 26 - ('a' - 'A')) % 26) + 'a';
        else if (encrypted[i] >= 'A' && encrypted[i] <= 'Z')
            decrypted[i] = (((encrypted[i] - newKey[i]) + 26) % 26) + 'A';
        else
            decrypted[i] = encrypted[i];
        // decrypted[i] = (((encrypted[i] - newKey[i]) + 26) % 26) + 'A';
    }

    for (; i < msgLen; ++i)
    {
        decrypted[i] = encrypted[i];
    }

    decrypted[i] = '\0';

    // strcpy(decrypted, enkripsi_atbash(decrypted));

    strcpy(encrypted, decrypted);
    return encrypted;
}

char *vigenere(char *msg)
{
    char key[] = "INNUGANTENG";
    int msgLen = strlen(msg), keyLen = strlen(key), i, j, extlen = 0;

    char *ext = strrchr(msg, '.');
    if (ext)
    {
        extlen = strlen(ext);
    }

    char newKey[msgLen], encrypted[msgLen];

    // generating new key
    for (i = 0, j = 0; i < msgLen; ++i, ++j)
    {
        if (j == keyLen)
            j = 0;

        newKey[i] = key[j];
    }

    newKey[i] = '\0';

    // encryption
    for (i = 0; i < msgLen - extlen; ++i)
    {
        if (msg[i] >= 'a' && msg[i] <= 'z')
            encrypted[i] = ((msg[i] + newKey[i] - ('a' - 'A')) % 26) + 'a';
        else if (msg[i] >= 'A' && msg[i] <= 'Z')
            encrypted[i] = ((msg[i] + newKey[i]) % 26) + 'A';
        else
            encrypted[i] = msg[i];
    }

    for (; i < msgLen; ++i)
    {
        encrypted[i] = msg[i];
    }

    encrypted[i] = '\0';
    strcpy(msg, encrypted);
    return msg;
}
```
![soal2](https://media.discordapp.net/attachments/964890423946543124/975402174844641311/unknown.png?width=987&height=660)

### 2 C D
Membuat file log dengan format INFO dan Warning. <br />
![soal2](https://cdn.discordapp.com/attachments/964890423946543124/975402775464804432/unknown.png)

### 3
Menggunakan pengkodean special.
```c
char *enspecial(char *filename)
{
    int length_extension = 0;
    char *extension = strrchr(filename, '.');
    if (extension)
    {
        length_extension = strlen(extension);
    }

    int pengkali = 1;
    int total = 0;
    for (int i = strlen(filename) - length_extension - 1; i >= 0; i--)
    {
        if (filename[i] >= 'a' && filename[i] <= 'z')
        {
            filename[i] = ('A' + (filename[i] - 'a'));
            total += pengkali;
        }
        pengkali = pengkali << 1;
    }

    // printf("%s.%d\n", filename, total);
    return filename;
}

char *decspecial(char *filename)
{
    int declen = 0, num = 0;
    char *dec = strrchr(filename, '.');
    if (dec)
    {
        declen = strlen(dec);
        num = atoi(dec + 1);
    }

    char temp[1000] = {0};
    strncpy(temp, filename, strlen(filename) - declen);
    temp[strlen(filename) - declen] = '\0';

    int extlen = 0;
    char *ext = strrchr(temp, '.');
    if (ext)
        extlen = strlen(ext);

    int ctr = 0;
    for (int i = strlen(temp) - extlen - 1; i >= 0; i--)
    {
        if ((1 << ctr) & num)
            temp[i] = 'a' + temp[i] - 'A';

        ctr++;
    }

    strcpy(filename, temp);
    return filename;
}
```
![spc](https://media.discordapp.net/attachments/964890423946543124/975408455751983154/unknown.png)